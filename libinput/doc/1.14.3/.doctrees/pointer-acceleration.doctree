��À      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _pointer-acceleration:�h]�h}�(h]�h]�h]�h]�h]��refid��pointer-acceleration�uhhhKhhhhh�@/home/whot/code/libinput/build/doc/user/pointer-acceleration.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Pointer acceleration�h]�h �Text����Pointer acceleration�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��libinput uses device-specific pointer acceleration methods, with the default
being the :ref:`ptraccel-linear`. The methods share common properties, such as
:ref:`ptraccel-velocity`.�h]�(h9�Wlibinput uses device-specific pointer acceleration methods, with the default
being the �����}�(h�Wlibinput uses device-specific pointer acceleration methods, with the default
being the �hhFhhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`ptraccel-linear`�h]�h �inline���)��}�(h�ptraccel-linear�h]�h9�ptraccel-linear�����}�(hhhhXubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhVhhRubah}�(h]�h]�h]�h]�h]��refdoc��pointer-acceleration��	refdomain�hd�reftype��ref��refexplicit���refwarn���	reftarget��ptraccel-linear�uhhPhh,hKhhFubh9�/. The methods share common properties, such as
�����}�(h�/. The methods share common properties, such as
�hhFhhhNhNubhQ)��}�(h�:ref:`ptraccel-velocity`�h]�hW)��}�(h�ptraccel-velocity�h]�h9�ptraccel-velocity�����}�(hhhh�ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhh}ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�h��reftype��ref��refexplicit���refwarn��hv�ptraccel-velocity�uhhPhh,hKhhFubh9�.�����}�(h�.�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h��This page explains the high-level concepts used in the code. It aims to
provide an overview for developers and is not necessarily useful for
users.�h]�h9��This page explains the high-level concepts used in the code. It aims to
provide an overview for developers and is not necessarily useful for
users.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _ptraccel-profiles:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-profiles�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Pointer acceleration profiles�h]�h9�Pointer acceleration profiles�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hKubhE)��}�(hX�  The profile decides the general method of pointer acceleration.
libinput currently supports two profiles: "adaptive" and "flat". The adaptive
profile is the default profile for all devices and takes the current speed
of the device into account when deciding on acceleration. The flat profile
is simply a constant factor applied to all device deltas, regardless of the
speed of motion (see :ref:`ptraccel-profile-flat`). Most of this document
describes the adaptive pointer acceleration.�h]�(h9X�  The profile decides the general method of pointer acceleration.
libinput currently supports two profiles: “adaptive” and “flat”. The adaptive
profile is the default profile for all devices and takes the current speed
of the device into account when deciding on acceleration. The flat profile
is simply a constant factor applied to all device deltas, regardless of the
speed of motion (see �����}�(hX�  The profile decides the general method of pointer acceleration.
libinput currently supports two profiles: "adaptive" and "flat". The adaptive
profile is the default profile for all devices and takes the current speed
of the device into account when deciding on acceleration. The flat profile
is simply a constant factor applied to all device deltas, regardless of the
speed of motion (see �hh�hhhNhNubhQ)��}�(h�:ref:`ptraccel-profile-flat`�h]�hW)��}�(h�ptraccel-profile-flat�h]�h9�ptraccel-profile-flat�����}�(hhhh�ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�h�reftype��ref��refexplicit���refwarn��hv�ptraccel-profile-flat�uhhPhh,hKhh�ubh9�E). Most of this document
describes the adaptive pointer acceleration.�����}�(h�E). Most of this document
describes the adaptive pointer acceleration.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�hhubh)��}�(h�.. _ptraccel-velocity:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-velocity�uhhhK"hh�hhhh,ubeh}�(h]�(�pointer-acceleration-profiles�h�eh]�h]�(�pointer acceleration profiles��ptraccel-profiles�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j  h�s�expect_referenced_by_id�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�Velocity calculation�h]�h9�Velocity calculation�����}�(hj%  hj#  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj   hhhh,hK!ubhE)��}�(hX  The device's speed of movement is measured across multiple input events
through so-called "trackers". Each event prepends a the tracker item, each
subsequent tracker contains the delta of that item to the current position,
the timestamp of the event that created it and the cardinal direction of the
movement at the time. If a device moves into the same direction, the
velocity is calculated across multiple trackers. For example, if a device
moves steadily for 10 events to the left, the velocity is calculated across
all 10 events.�h]�h9X  The device’s speed of movement is measured across multiple input events
through so-called “trackers”. Each event prepends a the tracker item, each
subsequent tracker contains the delta of that item to the current position,
the timestamp of the event that created it and the cardinal direction of the
movement at the time. If a device moves into the same direction, the
velocity is calculated across multiple trackers. For example, if a device
moves steadily for 10 events to the left, the velocity is calculated across
all 10 events.�����}�(hj3  hj1  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK#hj   hhubhE)��}�(hX  Whenever the movement changes direction or significantly changes speed, the
velocity is calculated from the direction/speed change only. For example, if
a device moves steadily for 8 events to the left and then 2 events to the
right, the velocity is only that of the last 2 events.�h]�h9X  Whenever the movement changes direction or significantly changes speed, the
velocity is calculated from the direction/speed change only. For example, if
a device moves steadily for 8 events to the left and then 2 events to the
right, the velocity is only that of the last 2 events.�����}�(hjA  hj?  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK,hj   hhubhE)��}�(hX  An extra time limit prevents events that are too old to factor into the
velocity calculation. For example, if a device moves steadily for 5 events
to the left, then pauses, then moves again for 5 events to the left, only
the last 5 events are used for velocity calculation.�h]�h9X  An extra time limit prevents events that are too old to factor into the
velocity calculation. For example, if a device moves steadily for 5 events
to the left, then pauses, then moves again for 5 events to the left, only
the last 5 events are used for velocity calculation.�����}�(hjO  hjM  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK1hj   hhubhE)��}�(h�>The velocity is then used to calculate the acceleration factor�h]�h9�>The velocity is then used to calculate the acceleration factor�����}�(hj]  hj[  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK6hj   hhubh)��}�(h�.. _ptraccel-factor:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-factor�uhhhK=hj   hhhh,ubeh}�(h]�(�velocity-calculation�j  eh]�h]�(�velocity calculation��ptraccel-velocity�eh]�h]�uhh-hh/hhhh,hK!j  }�jz  j  sj  }�j  j  subh.)��}�(hhh]�(h3)��}�(h�Acceleration factor�h]�h9�Acceleration factor�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hK<ubhE)��}�(hX  The acceleration factor is the final outcome of the pointer acceleration
calculations. It is a unitless factor that is applied to the current delta,
a factor of 2 doubles the delta (i.e. speeds up the movement), a factor of
less than 1 reduces the delta (i.e. slows the movement).�h]�h9X  The acceleration factor is the final outcome of the pointer acceleration
calculations. It is a unitless factor that is applied to the current delta,
a factor of 2 doubles the delta (i.e. speeds up the movement), a factor of
less than 1 reduces the delta (i.e. slows the movement).�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK>hj  hhubhE)��}�(hX(  Any factor less than 1 requires the user to move the device further to move
the visible pointer. This is called deceleration and enables high precision
target selection through subpixel movements. libinput's current maximum
deceleration factor is 0.3 (i.e. slow down to 30% of the pointer speed).�h]�h9X*  Any factor less than 1 requires the user to move the device further to move
the visible pointer. This is called deceleration and enables high precision
target selection through subpixel movements. libinput’s current maximum
deceleration factor is 0.3 (i.e. slow down to 30% of the pointer speed).�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKChj  hhubhE)��}�(h��A factor higher than 1 moves the pointer further than the physical device
moves. This is acceleration and allows a user to cross the screen quickly
but effectively skips pixels. libinput's current maximum acceleration factor
is 3.5.�h]�h9��A factor higher than 1 moves the pointer further than the physical device
moves. This is acceleration and allows a user to cross the screen quickly
but effectively skips pixels. libinput’s current maximum acceleration factor
is 3.5.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKHhj  hhubh)��}�(h�.. _ptraccel-linear:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-linear�uhhhKRhj  hhhh,ubeh}�(h]�(�acceleration-factor�js  eh]�h]�(�acceleration factor��ptraccel-factor�eh]�h]�uhh-hh/hhhh,hK<j  }�j�  ji  sj  }�js  ji  subh.)��}�(hhh]�(h3)��}�(h�Linear pointer acceleration�h]�h9�Linear pointer acceleration�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKQubhE)��}�(h��The linear pointer acceleration method is the default for most pointer
devices. It provides deceleration at very slow movements, a 1:1 mapping for
regular movements and a linear increase to the maximum acceleration factor
for fast movements.�h]�h9��The linear pointer acceleration method is the default for most pointer
devices. It provides deceleration at very slow movements, a 1:1 mapping for
regular movements and a linear increase to the maximum acceleration factor
for fast movements.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKShj�  hhubhE)��}�(h�~Linear pointer acceleration applies to devices with above 1000dpi resolution
and after :ref:`motion_normalization` is applied.�h]�(h9�WLinear pointer acceleration applies to devices with above 1000dpi resolution
and after �����}�(h�WLinear pointer acceleration applies to devices with above 1000dpi resolution
and after �hj�  hhhNhNubhQ)��}�(h�:ref:`motion_normalization`�h]�hW)��}�(h�motion_normalization�h]�h9�motion_normalization�����}�(hhhj�  ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�j  �reftype��ref��refexplicit���refwarn��hv�motion_normalization�uhhPhh,hKXhj�  ubh9� is applied.�����}�(h� is applied.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKXhj�  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�T.. figure:: ptraccel-linear.svg
    :align: center

    Linear pointer acceleration
�h]�h}�(h]�h]�h]�h]�h]��uri��ptraccel-linear.svg��
candidates�}��*�j6  suhj)  hj&  hh,hK^ubh �caption���)��}�(h�Linear pointer acceleration�h]�h9�Linear pointer acceleration�����}�(hj>  hj<  ubah}�(h]�h]�h]�h]�h]�uhj:  hh,hK^hj&  ubeh}�(h]��id2�ah]�h]�h]�h]��align��center�uhj$  hK^hj�  hhhh,ubhE)��}�(hXi  The image above shows the linear pointer acceleration settings at various
speeds. The line for 0.0 is the default acceleration curve, speed settings
above 0.0 accelerate sooner, faster and to a higher maximum acceleration.
Speed settings below 0 delay when acceleration kicks in, how soon the
maximum acceleration is reached and the maximum acceleration factor.�h]�h9Xi  The image above shows the linear pointer acceleration settings at various
speeds. The line for 0.0 is the default acceleration curve, speed settings
above 0.0 accelerate sooner, faster and to a higher maximum acceleration.
Speed settings below 0 delay when acceleration kicks in, how soon the
maximum acceleration is reached and the maximum acceleration factor.�����}�(hjU  hjS  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK`hj�  hhubhE)��}�(h�sExtremely low speed settings provide no acceleration and additionally
decelerate all movement by a constant factor.�h]�h9�sExtremely low speed settings provide no acceleration and additionally
decelerate all movement by a constant factor.�����}�(hjc  hja  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKfhj�  hhubh)��}�(h�.. _ptraccel-low-dpi:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-low-dpi�uhhhKnhj�  hhhh,ubeh}�(h]�(�linear-pointer-acceleration�j�  eh]�h]�(�linear pointer acceleration��ptraccel-linear�eh]�h]�uhh-hh/hhhh,hKQj  }�j�  j�  sj  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�(Pointer acceleration for low-dpi devices�h]�h9�(Pointer acceleration for low-dpi devices�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKmubhE)��}�(hXP  Low-dpi devices are those with a physical resolution of less than 1000 dots
per inch (dpi). The pointer acceleration is adjusted to provide roughly the
same feel for all devices at normal to high speeds. At slow speeds, the
pointer acceleration works on device-units rather than normalized
coordinates (see :ref:`motion_normalization`).�h]�(h9X3  Low-dpi devices are those with a physical resolution of less than 1000 dots
per inch (dpi). The pointer acceleration is adjusted to provide roughly the
same feel for all devices at normal to high speeds. At slow speeds, the
pointer acceleration works on device-units rather than normalized
coordinates (see �����}�(hX3  Low-dpi devices are those with a physical resolution of less than 1000 dots
per inch (dpi). The pointer acceleration is adjusted to provide roughly the
same feel for all devices at normal to high speeds. At slow speeds, the
pointer acceleration works on device-units rather than normalized
coordinates (see �hj�  hhhNhNubhQ)��}�(h�:ref:`motion_normalization`�h]�hW)��}�(h�motion_normalization�h]�h9�motion_normalization�����}�(hhhj�  ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�j�  �reftype��ref��refexplicit���refwarn��hv�motion_normalization�uhhPhh,hKohj�  ubh9�).�����}�(h�).�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKohj�  hhubj%  )��}�(hhh]�(j*  )��}�(h�b.. figure:: ptraccel-low-dpi.svg
    :align: center

    Pointer acceleration for low-dpi devices
�h]�h}�(h]�h]�h]�h]�h]��uri��ptraccel-low-dpi.svg�j7  }�j9  j�  suhj)  hj�  hh,hKxubj;  )��}�(h�(Pointer acceleration for low-dpi devices�h]�h9�(Pointer acceleration for low-dpi devices�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj:  hh,hKxhj�  ubeh}�(h]��id3�ah]�h]�h]�h]�jQ  �center�uhj$  hKxhj�  hhhh,ubhE)��}�(h��The image above shows the default pointer acceleration curve for a speed of
0.0 at different DPI settings. A device with low DPI has the acceleration
applied sooner and with a stronger acceleration factor.�h]�h9��The image above shows the default pointer acceleration curve for a speed of
0.0 at different DPI settings. A device with low DPI has the acceleration
applied sooner and with a stronger acceleration factor.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKzhj�  hhubh)��}�(h�.. _ptraccel-touchpad:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-touchpad�uhhhK�hj�  hhhh,ubeh}�(h]�(�(pointer-acceleration-for-low-dpi-devices�jy  eh]�h]�(�(pointer acceleration for low-dpi devices��ptraccel-low-dpi�eh]�h]�uhh-hh/hhhh,hKmj  }�j  jo  sj  }�jy  jo  subh.)��}�(hhh]�(h3)��}�(h�!Pointer acceleration on touchpads�h]�h9�!Pointer acceleration on touchpads�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hK�ubhE)��}�(hX.  Touchpad pointer acceleration uses the same approach as the
:ref:`ptraccel-linear` profile, with a constant deceleration factor applied. The
user expectation of how much a pointer should move in response to finger
movement is different to that of a mouse device, hence the constant
deceleration factor.�h]�(h9�<Touchpad pointer acceleration uses the same approach as the
�����}�(h�<Touchpad pointer acceleration uses the same approach as the
�hj&  hhhNhNubhQ)��}�(h�:ref:`ptraccel-linear`�h]�hW)��}�(h�ptraccel-linear�h]�h9�ptraccel-linear�����}�(hhhj3  ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhj/  ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�j>  �reftype��ref��refexplicit���refwarn��hv�ptraccel-linear�uhhPhh,hK�hj&  ubh9�� profile, with a constant deceleration factor applied. The
user expectation of how much a pointer should move in response to finger
movement is different to that of a mouse device, hence the constant
deceleration factor.�����}�(h�� profile, with a constant deceleration factor applied. The
user expectation of how much a pointer should move in response to finger
movement is different to that of a mouse device, hence the constant
deceleration factor.�hj&  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubj%  )��}�(hhh]�(j*  )��}�(h�c.. figure:: ptraccel-touchpad.svg
    :align: center

    Pointer acceleration curve for touchpads
�h]�h}�(h]�h]�h]�h]�h]��uri��ptraccel-touchpad.svg�j7  }�j9  ji  suhj)  hj[  hh,hK�ubj;  )��}�(h�(Pointer acceleration curve for touchpads�h]�h9�(Pointer acceleration curve for touchpads�����}�(hjm  hjk  ubah}�(h]�h]�h]�h]�h]�uhj:  hh,hK�hj[  ubeh}�(h]��id4�ah]�h]�h]�h]�jQ  �center�uhj$  hK�hj  hhhh,ubhE)��}�(h��The image above shows the touchpad acceleration profile in comparison to the
:ref:`ptraccel-linear`. The shape of the curve is identical but vertically squashed.�h]�(h9�MThe image above shows the touchpad acceleration profile in comparison to the
�����}�(h�MThe image above shows the touchpad acceleration profile in comparison to the
�hj�  hhhNhNubhQ)��}�(h�:ref:`ptraccel-linear`�h]�hW)��}�(h�ptraccel-linear�h]�h9�ptraccel-linear�����}�(hhhj�  ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�j�  �reftype��ref��refexplicit���refwarn��hv�ptraccel-linear�uhhPhh,hK�hj�  ubh9�>. The shape of the curve is identical but vertically squashed.�����}�(h�>. The shape of the curve is identical but vertically squashed.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubh)��}�(h�.. _ptraccel-trackpoint:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-trackpoint�uhhhK�hj  hhhh,ubeh}�(h]�(�!pointer-acceleration-on-touchpads�j	  eh]�h]�(�!pointer acceleration on touchpads��ptraccel-touchpad�eh]�h]�uhh-hh/hhhh,hK�j  }�j�  j�  sj  }�j	  j�  subh.)��}�(hhh]�(h3)��}�(h�#Pointer acceleration on trackpoints�h]�h9�#Pointer acceleration on trackpoints�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(hX�  The main difference between trackpoint hardware and mice or touchpads is
that trackpoint speed is a function of pressure rather than moving speed.
But trackpoint hardware is quite varied in how it reacts to user pressure
and unlike other devices it cannot easily be normalized for physical
properties. Measuring pressure objectively across a variety of hardware is
nontrivial. See :ref:`trackpoints` for more details.�h]�(h9X}  The main difference between trackpoint hardware and mice or touchpads is
that trackpoint speed is a function of pressure rather than moving speed.
But trackpoint hardware is quite varied in how it reacts to user pressure
and unlike other devices it cannot easily be normalized for physical
properties. Measuring pressure objectively across a variety of hardware is
nontrivial. See �����}�(hX}  The main difference between trackpoint hardware and mice or touchpads is
that trackpoint speed is a function of pressure rather than moving speed.
But trackpoint hardware is quite varied in how it reacts to user pressure
and unlike other devices it cannot easily be normalized for physical
properties. Measuring pressure objectively across a variety of hardware is
nontrivial. See �hj�  hhhNhNubhQ)��}�(h�:ref:`trackpoints`�h]�hW)��}�(h�trackpoints�h]�h9�trackpoints�����}�(hhhj�  ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�j�  �reftype��ref��refexplicit���refwarn��hv�trackpoints�uhhPhh,hK�hj�  ubh9� for more details.�����}�(h� for more details.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(hX�  The deltas for trackpoints are converted units/ms but there is no common
physical reference point for a unit. Thus, the same pressure on different
trackpoints will generate different speeds and thus different acceleration
behaviors. Additionally, some trackpoints provide the ability to adjust the
sensitivity in hardware by modifying a sysfs file on the serio node. A
higher sensitivity results in higher deltas, thus changing the definition of
what is a unit again.�h]�h9X�  The deltas for trackpoints are converted units/ms but there is no common
physical reference point for a unit. Thus, the same pressure on different
trackpoints will generate different speeds and thus different acceleration
behaviors. Additionally, some trackpoints provide the ability to adjust the
sensitivity in hardware by modifying a sysfs file on the serio node. A
higher sensitivity results in higher deltas, thus changing the definition of
what is a unit again.�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h��libinput attempts to normalize unit data to the best of its abilities, see
:ref:`trackpoint_multiplier`. Beyond this, it is not possible to have
consistent behavior across different touchpad devices.�h]�(h9�Klibinput attempts to normalize unit data to the best of its abilities, see
�����}�(h�Klibinput attempts to normalize unit data to the best of its abilities, see
�hj   hhhNhNubhQ)��}�(h�:ref:`trackpoint_multiplier`�h]�hW)��}�(h�trackpoint_multiplier�h]�h9�trackpoint_multiplier�����}�(hhhj-  ubah}�(h]�h]�(hc�std��std-ref�eh]�h]�h]�uhhVhj)  ubah}�(h]�h]�h]�h]�h]��refdoc�hp�	refdomain�j8  �reftype��ref��refexplicit���refwarn��hv�trackpoint_multiplier�uhhPhh,hK�hj   ubh9�`. Beyond this, it is not possible to have
consistent behavior across different touchpad devices.�����}�(h�`. Beyond this, it is not possible to have
consistent behavior across different touchpad devices.�hj   hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubj%  )��}�(hhh]�(j*  )��}�(h�h.. figure:: ptraccel-trackpoint.svg
    :align: center

    Pointer acceleration curves for trackpoints
�h]�h}�(h]�h]�h]�h]�h]��uri��ptraccel-trackpoint.svg�j7  }�j9  jc  suhj)  hjU  hh,hK�ubj;  )��}�(h�+Pointer acceleration curves for trackpoints�h]�h9�+Pointer acceleration curves for trackpoints�����}�(hjg  hje  ubah}�(h]�h]�h]�h]�h]�uhj:  hh,hK�hjU  ubeh}�(h]��id5�ah]�h]�h]�h]�jQ  �center�uhj$  hK�hj�  hhhh,ubhE)��}�(h�TThe image above shows the trackpoint acceleration profile for the speed in
units/ms.�h]�h9�TThe image above shows the trackpoint acceleration profile for the speed in
units/ms.�����}�(hj}  hj{  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _ptraccel-profile-flat:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-profile-flat�uhhhK�hj�  hhhh,ubeh}�(h]�(�#pointer-acceleration-on-trackpoints�j�  eh]�h]�(�#pointer acceleration on trackpoints��ptraccel-trackpoint�eh]�h]�uhh-hh/hhhh,hK�j  }�j�  j�  sj  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�%The flat pointer acceleration profile�h]�h9�%The flat pointer acceleration profile�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(h��In a flat profile, the acceleration factor is constant regardless of the
velocity of the pointer and each delta (dx, dy) results in an accelerated delta
(dx * factor, dy * factor). This provides 1:1 movement between the device
and the pointer on-screen.�h]�h9��In a flat profile, the acceleration factor is constant regardless of the
velocity of the pointer and each delta (dx, dy) results in an accelerated delta
(dx * factor, dy * factor). This provides 1:1 movement between the device
and the pointer on-screen.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _ptraccel-tablet:�h]�h}�(h]�h]�h]�h]�h]�h*�ptraccel-tablet�uhhhK�hj�  hhhh,ubeh}�(h]�(�%the-flat-pointer-acceleration-profile�j�  eh]�h]�(�%the flat pointer acceleration profile��ptraccel-profile-flat�eh]�h]�uhh-hh/hhhh,hK�j  }�j�  j�  sj  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Pointer acceleration on tablets�h]�h9�Pointer acceleration on tablets�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(h��Pointer acceleration for relative motion on tablet devices is a flat
acceleration, with the speed setting slowing down or speeding up the pointer
motion by a constant factor. Tablets do not allow for switchable profiles.�h]�h9��Pointer acceleration for relative motion on tablet devices is a flat
acceleration, with the speed setting slowing down or speeding up the pointer
motion by a constant factor. Tablets do not allow for switchable profiles.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubeh}�(h]�(�pointer-acceleration-on-tablets�j�  eh]�h]�(�pointer acceleration on tablets��ptraccel-tablet�eh]�h]�uhh-hh/hhhh,hK�j  }�j�  j�  sj  }�j�  j�  subeh}�(h]�(h+�id1�eh]�h]�(�pointer acceleration��pointer-acceleration�eh]�h]�uhh-hhhhhh,hKj  }�j  h sj  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j,  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`94b5be4`�h]�h �	reference���)��}�(h�git commit 94b5be4�h]�h9�git commit 94b5be4�����}�(h�94b5be4�hjl  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/94b5be4�uhjj  hjf  ubah}�(h]�h]�h]�jc  ah]�h]�uhjd  h�<rst_prolog>�hKhhub�git_version_full�je  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f8ef17fd830>`

�h]�jk  )��}�(h�<git commit <function get_git_version_full at 0x7f8ef17fd830>�h]�h9�<git commit <function get_git_version_full at 0x7f8ef17fd830>�����}�(h�1<function get_git_version_full at 0x7f8ef17fd830>�hj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f8ef17fd830>�uhjj  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhjd  hj�  hKhhubu�substitution_names�}�(�git_version�jc  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ah�]�h�aj  ]�j  ajs  ]�ji  aj�  ]�j�  ajy  ]�jo  aj	  ]�j�  aj�  ]�j�  aj�  ]�j�  aj�  ]�j�  au�nameids�}�(j  h+j  j   j  h�j  j  jz  j  jy  jv  j�  js  j�  j�  j�  j�  j  j|  j  jy  j  j  j�  j	  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  u�	nametypes�}�(j  �j  Nj  �j  Njz  �jy  Nj�  �j�  Nj�  �j  Nj  �j  Nj�  �j�  Nj�  �j�  Nj�  �j�  Nj�  �j�  Nuh}�(h+h/j   h/h�h�j  h�j  j   jv  j   js  j  j�  j  j�  j�  j|  j�  jy  j�  j  j�  j	  j  j�  j  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  jL  j&  j�  j�  j{  j[  ju  jU  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "pointer-acceleration" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�7Hyperlink target "ptraccel-profiles" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�7Hyperlink target "ptraccel-velocity" is not referenced.�����}�(hhhj
  ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K"uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�5Hyperlink target "ptraccel-factor" is not referenced.�����}�(hhhj$  ubah}�(h]�h]�h]�h]�h]�uhhDhj!  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K=uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�5Hyperlink target "ptraccel-linear" is not referenced.�����}�(hhhj>  ubah}�(h]�h]�h]�h]�h]�uhhDhj;  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�KRuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "ptraccel-low-dpi" is not referenced.�����}�(hhhjX  ubah}�(h]�h]�h]�h]�h]�uhhDhjU  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Knuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�7Hyperlink target "ptraccel-touchpad" is not referenced.�����}�(hhhjr  ubah}�(h]�h]�h]�h]�h]�uhhDhjo  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�9Hyperlink target "ptraccel-trackpoint" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�;Hyperlink target "ptraccel-profile-flat" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�5Hyperlink target "ptraccel-tablet" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ube�transformer�N�
decoration�Nhhub.