���$      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _middle_button_emulation:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��middle-button-emulation�uh0h]h:Kh hhhh8�C/home/whot/code/libinput/build/doc/user/middle-button-emulation.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Middle button emulation�h]�h�Middle button emulation�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��Middle button emulation provides users with the ability to generate a middle
click even when the device does not have a physical middle button available.�h]�h��Middle button emulation provides users with the ability to generate a middle
click even when the device does not have a physical middle button available.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��When middle button emulation is enabled, a simultaneous press of the left
and right button generates a middle mouse button event. Releasing the
buttons generates a middle mouse button release, the left and right button
events are discarded otherwise.�h]�h��When middle button emulation is enabled, a simultaneous press of the left
and right button generates a middle mouse button event. Releasing the
buttons generates a middle mouse button release, the left and right button
events are discarded otherwise.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K
h hnhhubh�)��}�(h��The middle button release event may be generated when either button is
released, or when both buttons have been released. The exact behavior is
device-dependent, libinput will implement the behavior that is most
appropriate to the physical device.�h]�h��The middle button release event may be generated when either button is
released, or when both buttons have been released. The exact behavior is
device-dependent, libinput will implement the behavior that is most
appropriate to the physical device.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX�  The middle button emulation behavior when combined with other device
buttons, including a physical middle button is device-dependent.
For example, :ref:`clickpad_softbuttons` provides a middle button area when
middle button emulation is disabled. That middle button area disappears
when middle button emulation is enabled - a middle click can then only be
triggered by a simultaneous left + right click.�h]�(h��The middle button emulation behavior when combined with other device
buttons, including a physical middle button is device-dependent.
For example, �����}�(h��The middle button emulation behavior when combined with other device
buttons, including a physical middle button is device-dependent.
For example, �h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`clickpad_softbuttons`�h]�h �inline���)��}�(hh�h]�h�clickpad_softbuttons�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�hʌrefexplicit���	reftarget��clickpad_softbuttons��refdoc��middle-button-emulation��refwarn��uh0h�h8hkh:Kh h�ubh�� provides a middle button area when
middle button emulation is disabled. That middle button area disappears
when middle button emulation is enabled - a middle click can then only be
triggered by a simultaneous left + right click.�����}�(h�� provides a middle button area when
middle button emulation is disabled. That middle button area disappears
when middle button emulation is enabled - a middle click can then only be
triggered by a simultaneous left + right click.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX  Some devices provide middle mouse button emulation but do not allow
enabling/disabling that emulation. Likewise, some devices may allow middle
button emulation but have it disabled by default. This is the case for most
mouse-like devices where a middle button is detected.�h]�hX  Some devices provide middle mouse button emulation but do not allow
enabling/disabling that emulation. Likewise, some devices may allow middle
button emulation but have it disabled by default. This is the case for most
mouse-like devices where a middle button is detected.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX  libinput provides **libinput_device_config_middle_emulation_set_enabled()** to
enable or disable middle button emulation. See :ref:`faq_configure_wayland`
and :ref:`faq_configure_xorg` for info on how to enable or disable middle
button emulation in the Wayland compositor or the X stack.�h]�(h�libinput provides �����}�(h�libinput provides �h h�hhh8Nh:Nubh �strong���)��}�(h�9**libinput_device_config_middle_emulation_set_enabled()**�h]�h�5libinput_device_config_middle_emulation_set_enabled()�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j   h h�ubh�3 to
enable or disable middle button emulation. See �����}�(h�3 to
enable or disable middle button emulation. See �h h�hhh8Nh:Nubh�)��}�(h�:ref:`faq_configure_wayland`�h]�h�)��}�(hj  h]�h�faq_configure_wayland�����}�(hhh j  ubah!}�(h#]�h%]�(hɌstd��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j#  �refexplicit��hٌfaq_configure_wayland�h�h�h݈uh0h�h8hkh:K h h�ubh�
and �����}�(h�
and �h h�hhh8Nh:Nubh�)��}�(h�:ref:`faq_configure_xorg`�h]�h�)��}�(hj:  h]�h�faq_configure_xorg�����}�(hhh j<  ubah!}�(h#]�h%]�(hɌstd��std-ref�eh']�h)]�h+]�uh0h�h j8  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jF  �refexplicit��hٌfaq_configure_xorg�h�h�h݈uh0h�h8hkh:K h h�ubh�g for info on how to enable or disable middle
button emulation in the Wayland compositor or the X stack.�����}�(h�g for info on how to enable or disable middle
button emulation in the Wayland compositor or the X stack.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K h hnhhubeh!}�(h#]�(hj�id1�eh%]�h']�(�middle button emulation��middle_button_emulation�eh)]�h+]�uh0hlh hhhh8hkh:K�expect_referenced_by_name�}�jg  h_s�expect_referenced_by_id�}�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�hj]�h_as�nameids�}�(jg  hjjf  jc  u�	nametypes�}�(jg  �jf  Nuh#}�(hjhnjc  hnu�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�=Hyperlink target "middle-button-emulation" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  uba�transformer�N�
decoration�Nhhub.